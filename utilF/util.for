!**************************************************************
!	Utility subroutines for
!	2-Dimensional Debris Flow Simulation debris2d.for
!
!	Author:
!		Dr. Kiyoshi Honda
!
!	Copyright Owners:
!		Dr. Kiyoshi Honda, Ringyo Doboku Consultants, 2003
!
!
!	Use, Modification and Distribution:
!		Use, modification and distribution of ths software needs
!		written agreement from the author or the copyright holders.
!	
!	Contact:
!		Dr. Kiyoshi Honda:
!			honda.kiyoshi@usa.net, honda@ait.ac.th
!		
!		Ringyo Doboku Consultants:
!			Mr. Masaaki Sakurai: m-sakurai@jfec.or.jp
!
!	Responsibility:
!		Any authors or copyright holders do not give any
!		gurantee for the accuracy of the calculation result
!		of this software.
!		Any authors or copyright holders are not responsible
!		for the loss or damages caused by the use of this software
!
!	Output File Format:
!		Results are stored in files in ENVI format, which is
!		associated with .hdr header file.
!		ENVI image files can be viewd by "freelook" free software
!		distributed at	http://www.rsinc.com/envi/.
!		The format is simple flat binary. Simply open .hdr
!		file with text viewer and the size of the file can be
!		easily interpreted. Byteorder and data type is as follows
!			byte order:		0: LSB 1st	1: MSB 1st
!			data type:		1: unsigned char, 4: float
!
!			
!****************************************************************

c*********************************************************
	subroutine imageSaveI1I1( fname, x, dum1, dum2, nx, ny, MMX, MMY,
     +						  dx, dy )
      INCLUDE 'globalparameter.inc'
	character*(*) fname
	integer	MMX, MMY, nx, ny
	integer*1 x(MMX,MMY)
	real*8	dum1, dum2, dx, dy
c*********************************************************

	integer*1 buf(MY)
	integer	i,j,k
	integer	nchan	/ 1 /

	if( MMY .gt. MY ) stop 'imgsaveI1I1 MMY>MY'

	open( 10, status='UNKNOWN', file=fname, 
     +	FORM='BINARY')
	do k=1,nchan
	do i=1,nx
		do j=1,ny
			buf(j)=x(i,j)
		end do
		write(10) (buf(j), j=1,ny)
	end do
	end do
	close(10)

	call writeENVIHeader( fname, IMAGE_CHAR, nx, ny, 1, dx, dy )
	
	end

c*********************************************************
	subroutine imageSaveI4I1( fname, x, xmin, xmax, nx, ny, MMX, MMY,
     +						  dx, dy )
      INCLUDE 'globalparameter.inc'
	character*(*) fname
	integer	MMX, MMY, nx, ny
	integer*4 x(MMX,MMY)
	real*8	xmin, xmax, dx, dy
c*********************************************************

	integer*1 buf(MY)
	integer	i,j,k
	integer	nchan	/ 1 /
	if( MMY .gt. MY ) stop 'imgsaveI4I1 MMY>MY'

	open( 10, status='UNKNOWN', file=fname, 
     +	FORM='BINARY')

	do k=1,nchan
	do i=1,nx
		do j=1,ny
			if( x(i,j) .lt. xmin ) then
				buf(j)=0
			else if( x(i,j) .gt. xmax ) then
				buf(j)=255
			else
				buf(j)=int((x(i,j)-xmin)/(xmax-xmin)*255.+0.5)
			endif
		end do
		write(10) (buf(j), j=1,ny)
	end do
	end do
	close(10)

	call writeENVIHeader( fname, IMAGE_CHAR, nx, ny, 1, dx, dy )

	end

c*********************************************************
	subroutine imageSaveR8I1( fname, x, xmin, xmax, nx, ny, MMX, MMY,
     +						  dx, dy )
      INCLUDE 'globalparameter.inc'

c	save real*8 image to char image
c	xmin-xmax is sacled to 0-200

	character*(*) fname
	integer MMX, MMY, nx, ny
	real*8 x(MMX,MMY), xmin, xmax, dx, dy
c*********************************************************

	integer*1 buf(MY)
	integer	i,j,k
	integer	nchan	/ 1 /
	if( MMY .gt. MY ) stop 'imgsaveR8I1 MMY>MY'

	open( 10, status='UNKNOWN', file=fname, 
     +	FORM='BINARY')

	do k=1,nchan
	do i=1,nx
		do j=1,ny
			if( x(i,j) .lt. xmin ) then
				buf(j)=0
			else if( x(i,j) .gt. xmax ) then
				buf(j)=255
			else
				buf(j)=int((x(i,j)-xmin)/(xmax-xmin)*200.)
			endif
		end do
		write(10) (buf(j), j=1,ny)
	end do
	end do
	close(10)

	call writeENVIHeader( fname, IMAGE_CHAR, nx, ny, 1, dx, dy )

	end

c*********************************************************
	subroutine imageSaveR8R4( fname, x, dum1, dum2, nx, ny, MMX, MMY,
     +						  dx, dy )
      INCLUDE 'globalparameter.inc'

c	save real*8 image to r*4(float) image
c	write out ENVI type Header

	character*(*) fname
	integer MMX, MMY, nx, ny
	real*8 x(MMX,MMY), dum1, dum2, dx, dy
c*********************************************************

	real*4 buf(MY)
	integer	i,j,k
	if( MMY .gt. MY ) stop 'imgsaveR8R4 MMY>MY'

	open( 10, status='UNKNOWN', file=fname, 
     +	FORM='BINARY')
	do i=1,nx
		do j=1,ny
			buf(j)=x(i,j)
		end do
		write(10) (buf(j), j=1,ny)
	end do
	close(10)

	call writeENVIHeader( fname, IMAGE_FLOAT, nx, ny, 1, dx, dy )

	end

c*********************************************************
	subroutine imageReadI1I1( fname, x, dum1, dum2, nx, ny, MMX, MMY,
     +						  dx, dy )
      INCLUDE 'globalparameter.inc'

c	read I*1 ENVI image and store to I*1 array x
c	currently only 1 channel image
c	read  ENVI type Header

	character*(*) fname
	integer MMX, MMY, nx, ny
	integer*1 x(MMX,MMY)
	real*8	dum1, dum2, dx, dy
	integer	dataType, nchan
c*********************************************************

      integer*1 buf(MY)
	integer	i,j,k

	call readENVIHeader( fname, dataType, nx, ny, nchan, dx, dy )

	if( dataType .ne. IMAGE_CHAR ) then
		stop 'imageReadI1I1: image type is not char'
	endif

	if( nchan .ne. 1 ) then
		stop 'imageReadI1I1: nchan must be 1'
	endif

	if( nx .gt. MMX ) stop 'imageReadI1I1 nx>MMX'
	if( ny .gt. MMY ) stop 'imageReadI1I1 ny>MMY'
	if( ny .gt. MY  ) stop 'imageReadI1I1 ny>MY'

	open( 10, status='OLD', file=fname, 
     +	FORM='BINARY')
	do i=1,nx
		read(10) (buf(j), j=1,ny)
		do j=1,ny
			x(i,j)=buf(j)
		end do
	end do
	close(10)

	end

c*********************************************************
	subroutine imageReadI4I1( fname, x, dum1, dum2, nx, ny, MMX, MMY,
     +						  dx, dy )
      INCLUDE 'globalparameter.inc'

c	read I*1 ENVI image and store to I*4 array x
c	currently only 1 channel image
c	read  ENVI type Header

	character*(*) fname
	integer MMX, MMY, nx, ny
	integer*4 x(MMX,MMY)
	real*8	dum1, dum2, dx, dy
	integer	dataType, nchan
c*********************************************************

	integer*1 buf(MY)
	integer	i,j,k

	call readENVIHeader( fname, dataType, nx, ny, nchan, dx, dy )

	if( dataType .ne. IMAGE_CHAR ) then
		stop 'imageReadI1I1: image type is not char'
	endif

	if( nchan .ne. 1 ) then
		stop 'imageReadI1I1: nchan must be 1'
	endif

	if( nx .gt. MMX ) stop 'imageReadI1I1 nx>MMX'
	if( ny .gt. MMY ) stop 'imageReadI1I1 ny>MMY'
	if( ny .gt. MY  ) stop 'imageReadI1I1 ny>MY'

	open( 10, status='OLD', file=fname, 
     +	FORM='BINARY')
	do i=1,nx
		read(10) (buf(j), j=1,ny)
		do j=1,ny
			x(i,j)=buf(j)
		end do
	end do
	close(10)

	end
c*********************************************************
	subroutine imageReadR8R4( fname, x, dum1, dum2, nx, ny, MMX, MMY,
     +						  dx, dy )
      INCLUDE 'globalparameter.inc'

c	read r*4(float) ENVI image file and store to real*8 data x
c	currently only 1 channel image
c	read  ENVI type Header

	character*(*) fname
	integer MMX, MMY, nx, ny
	real*8 x(MMX,MMY), dum1, dum2, dx, dy
	integer	dataType, nchan
c*********************************************************

	real*4 buf(MY)
	integer	i,j,k

	call readENVIHeader( fname, dataType, nx, ny, nchan, dx, dy )

	if( dataType .ne. IMAGE_FLOAT ) then
		stop 'imgreadR8R4: image type is not float'
	endif

	if( nchan .ne. 1 ) then
		stop 'imgreadR8R4: nchan must be 1'
	endif

	if( nx .gt. MMX ) stop 'imgreadR8R4 nx>MMX'
	if( ny .gt. MMY ) stop 'imgreadR8R4 ny>MMY'
	if( ny .gt. MY  ) stop 'imgreadR8R4 ny>MY'

	open( 10, status='OLD', file=fname, 
     +	FORM='BINARY')
	do i=1,nx
		read(10) (buf(j), j=1,ny)
		do j=1,ny
			x(i,j)=buf(j)
		end do
	end do
	close(10)

	end
C**********************************************************************
	subroutine writeENVIHeader( fname, dataType, nx, ny, nchan, dx, dy )

      INCLUDE 'globalparameter.inc'
	integer	dataType, nx, ny, nchan
	real*8	dx, dy
	character*(*) fname
C**********************************************************************
	character*256	fnameEnvi

	integer	dataTypeEnvi

	if( dataType .eq. IMAGE_CHAR ) then
		dataTypeEnvi = ENVI_IMAGE_CHAR
	else if( dataType .eq. IMAGE_FLOAT ) then
		dataTypeEnvi = ENVI_IMAGE_FLOAT
	else
		stop 'writeENVIHeader: illegal data type'
	endif

!	fprintf(envi_fp, "ENVI\n");
!	fprintf(envi_fp, "samples = %d\n", imgf->w );
!	fprintf(envi_fp, "lines   = %d\n", imgf->h );
!	fprintf(envi_fp, "bands   = %d\n", imgf->nchan );
!	fprintf(envi_fp, "data type = 1\n" );
!	fprintf(envi_fp, "interleave = bil\n" );
!	fprintf(envi_fp, "interleave = bsq\n" );
!	fprintf(envi_fp, "header offset = %d\n", imgf->header_length );
!		case IMAGE_LSB_1ST:
!			fprintf(envi_fp, "byte order = 0\n" );
!			break;
!		case IMAGE_MSB_1ST:
!			fprintf(envi_fp, "byte order = 1\n" );

	fnameEnvi=fname(1:len_trim(fname))//'.hdr'
	open(10, status='UNKNOWN', file=fnameEnvi )

	write(10,'(a)') 'ENVI'
	write(10,'("samples =", i5)') ny
	write(10,'("lines   =", i5)') nx
	write(10,'("bands   =", i5)') nchan
	write(10,'("data type = ", i5)') dataTypeEnvi
	write(10,'("interleave = bsq")')
	write(10,'("header offset = 0")')
	if( BYTE_ORDER_THIS_MACHINE .eq. IMAGE_LSB_1ST ) then
		write(10,'("byte order = 0")')
	else
		write(10,'("byte order = 1")')
	endif
	write(10,'("pixel size = { ",f12.6, 1h,, f12.6, "}")'), dx, dy

	close(10)
	end

C**********************************************************************
	subroutine readENVIHeader( fname, dataType, nx, ny, nchan, dx, dy )

      INCLUDE 'globalparameter.inc'
	integer	dataType, nx, ny, nchan
	real*8	dx, dy
	character*(*) fname
C**********************************************************************
	character*256	fnameEnvi, buf

	integer	dataTypeEnvi
	integer	byteOrder, byteOrderNum
	character*32	interLeaveChar

	fnameEnvi=fname(1:len_trim(fname))//'.hdr'
	open(10, status='OLD', file=fnameEnvi )

	byteOrder = BYTE_ORDER_THIS_MACHINE
	nx = -1
	ny = -1
	nchan = -1
	dataType = IMAGE_CHAR
	dx = -1.
	dy = -1.

	read(10, '(a)') buf
	if( .not. bufcomp( buf, 'ENVI' ) ) then
		write(*,*)'readENVIHeader: First line is not ENVI'
		stop
	endif
	do while( .TRUE. )
		read(10,'(a)', end=99) buf
		print *,buf(1:40)
		if ( bufComp2(buf, 'samples') ) then
			call ivalGet( buf, ny )
			write(*,'(a, i12)') 'ny=',ny
		else if ( bufComp2(buf, 'lines') ) then
			call ivalGet( buf, nx )
			write(*,'(a, i12)') 'nx=',nx
		else if ( bufComp2(buf, 'bands') ) then
			call ivalGet( buf, nchan )
			write(*,'(a, i12)') 'nchan =',nchan
		else if ( bufComp2(buf, 'data type') ) then
			call ivalGet( buf, dataTypeEnvi )
			write(*,'(a, i12)') 'dataTypeEnvi=',dataTypeEnvi
			if( dataTypeEnvi .eq. ENVI_IMAGE_CHAR ) then
				dataType = IMAGE_CHAR
			else if( dataTypeEnvi .eq. ENVI_IMAGE_FLOAT ) then
				dataType = IMAGE_FLOAT
			else
				write(*,*)
     +		'readENVIHeader: dataType not supported',dataTypeEnvi
				stop 'readENVIHeader: illegal data type'
			endif
		else if ( bufComp2(buf, 'interleave') ) then
			call stringGet( buf, interLeaveChar )
			if( interLeaveChar .ne. 'bsq' ) then
		write(*,*) 'readENVIHeader: warning: interleave is not bsq'
c				stop'readENVIHeader: interleave must be bsq'
			endif
		else if ( bufComp2(buf, 'byte order') ) then
			call ivalGet( buf, byteOrderNum )
			if( byteOrderNum .eq. 0 ) then
				byteOrder=IMAGE_LSB_1ST
			else if( byteOrderNum .eq. 0 ) then
				byteOrder=IMAGE_MSB_1ST
			else
				stop'readENVIHeader: illegal byteOrder Number'
			endif
		else if ( bufComp2(buf, 'pixel size') ) then
			call valGet( buf, dy )
			call valGet( buf, dx )
			write(*,'("dx,dy=",2f12.5)') dx, dy
		endif
	end do
   99 continue

	if( byteOrder .ne. BYTE_ORDER_THIS_MACHINE ) then
		if( dataType .ne. IMAGE_CHAR ) then
			write(*,*)'readENVIHeader: Byteorder should be native'
			stop
		endif
	endif
	if( nx .lt. 1  .or.  ny .lt. 1 ) then
		write(*,*)'readENVIHeader: size was not set'
		stop
	endif
	if( nchan .lt. 1   ) then
		write(*,*)'readENVIHeader: nchan was not set'
		stop
	endif
	if( nchan .gt. 1  .and.  interLeaveChar .ne. 'bsq' ) then
		write(*,*) 'readENVIHeader: bil multichannel not supported'
		stop
	endif

	close(10)
	end

c*********************************************************
	logical function bufComp2( buf, sub)
	character*(*) buf, sub
c	Compare if the buf starts from sub
c	buf and sub will be converted to lower case and compared
c	The leading spaces will be neglected
c	If the first part matches to sub, this part will
c	be removed from buf.
c*********************************************************

	call cleanStr(buf)
	call cleanStr(sub)

	buf=adjustl( buf )
	call lower( buf )
	call lower( sub )

	if (index( buf, sub ) .eq. 1 ) then
		bufComp2=.TRUE.
		buf=buf(len_trim(sub)+1:)
	else
		bufComp2=.false.
	endif
	end

c*********************************************************
	logical function bufComp( buf, sub)
	character*(*) buf, sub
c	Compare if the 1st token in the buf exactly match to sub
c	buf and sub will be converted to lower case and compared
c	The leading spaces will be neglected
c	If the first part matches to sub, this part will
c	be removed from buf.
c*********************************************************

	character*1024 token
	logical getToken

	call cleanStr(buf)
	call cleanStr(sub)

	buf=adjustl( buf )
	call lower( buf )
	call lower( sub )

	if( .not. getToken(buf, token) ) then
		bufComp=.false.
		return
	endif

	if ( token .eq. sub ) then
		bufComp=.TRUE.
	else
		buf=token(1:len_trim(token))//' '//buf
		bufComp=.false.
	endif
	end

c*********************************************************
	subroutine lower( buf )
	character*(*) buf
c	Convert upper case to lower case
c*********************************************************
	integer	dif, i

	dif=ichar('a')-ichar('A')
	do i=1, len_trim(buf)
		if( ichar(buf(i:i)) .ge. ichar('A')   .and.
     +		ichar(buf(i:i)) .le. ichar('Z')        ) then
			buf(i:i)=char(ichar(buf(i:i))+dif )
		endif
	end do
	end

c*********************************************************
	subroutine valGet( buf, val )
	character*(*) buf
	real*8	val
c	Convert the first token in the buf to real*8
c	The token will be removed from buf.
c*********************************************************

	logical getToken

	character*1024	token

	if( getToken(buf, token) ) then
		read( token, *, err=99) val
	else
		write(*,*)'valGet: no token: ', buf(1:len_trim(buf))		
	endif
	return

   99 continue
	write(*,*)'valGet: read error: ', token(1:len_trim(token))
	return

	end

c*********************************************************
	subroutine ivalGet( buf, val )
	character*(*) buf
	integer	val
c	Convert the first token in the buf to integer
c	The token will be removed from buf.
c*********************************************************

	logical getToken

	character*1024	token

	if( getToken(buf, token) ) then
		read( token, *, err=99) val
	else
		write(*,*)'valGet: no token: ', buf(1:len_trim(buf))		
	endif
	return

   99 continue
	write(*,*)'valGet: read error: ', token(1:len_trim(token))
	return

	end

c*********************************************************
	subroutine stringGet( buf, string )
	character*(*) buf, string
c	Set the first token to the string.
c	The token will be removed from buf.
c*********************************************************

	character*1024	token

	logical getToken

	if( getToken(buf, token) ) then
		string=token
	else
		write(*,*)'stringGet: no token: ', buf(1:len_trim(buf))		
	endif
	return

   99 continue
	write(*,*)'valGet: read error: ', buf(1:len_trim(buf))
	return
	end


c*********************************************************
	logical function getToken( buf, token )

	character*(*) buf
	character*(*) token
c	Find a token separated by spaces or commas or =
c	in buf and remove it from buf.
c	if token is found return .true. else return .false.
c*********************************************************
	integer	i

	getToken=.false.

c	clean un-readable character and replace ,= to a space
	call cleanStr(buf)
	call lower(buf)
	buf=adjustl(buf)

c	write(*,*)'getToken', buf(1:len_trim(buf)), len_trim(buf)
	if( len_trim(buf) .eq. 0 ) then
		return
	endif

	token=buf
	getToken=.true.
	do i=1, len_trim(buf)
c		write(*,*) i, ichar(' '), ichar(buf(i:i))
		if( ichar(buf(i:i))  .le. ichar(' ')  ) then
			token=buf(1:i-1)
			buf=buf(i+1:)
			buf=adjustl(buf)
c			write(*,*) 'token=', token
			return
		endif
	end do
	buf=' '

	end

c*********************************************************
	subroutine cleanStr( buf )
	character*(*) buf
c	Replace non-readable characters such as tabs to spaces
c	Also replace separater , = { } to a space
c*********************************************************
	integer	i

	do i=1, len(buf)
		if( ichar(buf(i:i)) .le. ichar(' ' ))then
			buf(i:i)=' '
		else if( ichar(buf(i:i)) .eq. ichar(',' ))then
			buf(i:i)=' '
		else if( ichar(buf(i:i)) .eq. ichar('=' ))then
			buf(i:i)=' '
		else if( ichar(buf(i:i)) .eq. ichar('{' ))then
			buf(i:i)=' '
		else if( ichar(buf(i:i)) .eq. ichar('}' ))then
			buf(i:i)=' '
		endif
	end do
	end