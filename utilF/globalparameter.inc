	IMPLICIT REAL*8(a-h,o-z)
	IMPLICIT INTEGER*4(i-n)

c	pi		: =3.1415.....
	real*8	pi, gravity
	parameter( pi=3.14159265358979323846 )
	parameter( gravity=9.8 )

	integer MX, MY
	parameter( MX=440, MY=320 )

	real*8 EPS
	Parameter(EPS=0.00001)

c	parameters to indicate dataType : same as image.h

	integer	IMAGE_CHAR, IMAGE_FLOAT
	parameter ( IMAGE_CHAR=2, IMAGE_FLOAT=7 )

!
!	#define IMAGE_PLANE     1
!	#define IMAGE_CHAR      2
!	#define IMAGE_SHORT     3
!	#define IMAGE_USHORT    4
!	#define IMAGE_LONG      5
!	#define IMAGE_ULONG     6
!	#define IMAGE_FLOAT     7
!	#define IMAGE_DOUBLE    8
!	#define IMAGE_COMPLEX   9
!	#define IMAGE_DCOMPLEX  10
!	#define IMAGE_8BIT_COL  11
!	#define IMAGE_24BIT_COL 12
!	#define IMAGE_32BIT_COL 13
!	#define IMAGE_UNKNOWN   -1

c	parameters to indicate dataType in ENVI header
	integer	ENVI_IMAGE_CHAR, ENVI_IMAGE_FLOAT
	parameter ( ENVI_IMAGE_CHAR=1, ENVI_IMAGE_FLOAT=4 )

!	#define	IMAGE_MSB_1ST	1
!	#define	IMAGE_LSB_1ST	2
!*********************************************************
!	IF WORKSTATION: CHANGE TO IMAGE_MSB_1ST
!*********************************************************

	integer	IMAGE_MSB_1ST, IMAGE_LSB_1ST, BYTE_ORDER_THIS_MACHINE
	parameter( IMAGE_MSB_1ST=1, IMAGE_LSB_1ST=2)

	parameter( BYTE_ORDER_THIS_MACHINE=IMAGE_LSB_1ST )

	logical bufComp2, bufComp
