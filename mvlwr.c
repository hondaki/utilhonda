#include<stdio.h>
#include<string.h>
#include<stdlib.h>

int main( int argc, char **argv )
{
	char	newFileName[1024];
	char	oldFileName[1024];
	char	command[1024];
	int	i;

	argc--; argv++;
	for(i=0; i<argc; i++){
		strcpy( oldFileName, argv[i] );	
		_strlwr2( oldFileName );
		strcpy( newFileName, argv[i] );	
		_strlwr( newFileName );
		strcpy( command, "mv " );
		strcat( command, oldFileName );
		strcat( command, " ");
		strcat( command, newFileName );
		printf( "%s\n", command );
	}
}
_strlwr( char *s )
{
	int	i;
	while(*s){
		if( *s >= 'A' && *s <='Z' ){
			*s = *s+'a'-'A';
		}
		s++;
	}
}

_strlwr2( char *s )
{
	int	i;
	int	len;
	len = strlen(s);
	while( len-- ){
		if( *(s+len) == '/' ){
			break;
		}
	}
	if( len <= 0 )
		len = strlen(s);

	for(i=0; i<len; i++ ){
		if( *(s+i) >= 'A' && *(s+i) <='Z' ){
			*(s+i) += 'a'-'A';
		}
	}
}
