#include<stdio.h>
#include<ctype.h>

void usage(void)
{
	fprintf(stderr,"fileDump file\n");
}

int main( int argc, char *argv[] )
{
	char	*fname;
	int	i, count;
	FILE	*fp;
	unsigned char	buf[1025];

	if( argc != 2 ){
		usage();
		return 1;
	}
	fname = argv[1];

	fp = fopen( fname, "rb" );
	while(	(count = fread( buf, 1, 16, fp )) > 0 ){
		for(i=0; i< count; i++ ){
				printf("%2.2x ", buf[i] );
		}
		for(i=count; i< 16; i++ ){
			printf("   " );
		}
		printf("   " );
		for(i=0; i< count; i++ ){
			if( isprint( buf[i] ) )
				printf("%c",buf[i]);
			else
				printf(".");
		}
		for(i=count; i< 16; i++ ){
			printf(" ");
		}
		printf("\n");
	}
	fclose(fp);

}
