#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include"image.h"
#include"imgio.h"


void usage();
int surferAsciiGrid2ENVI( char *imgFname1 );
static int removeExtention2( char *fname );
char *pureFileName( char *fname );

main( int argc, char *argv[])
{
	char	*imgFname;
	int	i;

	if( argc < 2 ){
		usage();
		exit(1);
	}

	for(i=1; i<argc; i++ ){
		imgFname=argv[i];
		surferAsciiGrid2ENVI( imgFname );
	}
}

void usage()
{
	fprintf(stderr, "surferAsciiGrid2ENVI  GRID_files\n"
					"surferAsciiGrid2ENVI *.grd");
}



#define	MAXBUFL	32000

int surferAsciiGrid2ENVI( char *imgFname1 )
{
	FILE		*imgf1;
	IMAGE_FILE	*imgf2;
	IMAGE		*img2;
	char		imgFname2[1024];
	char		imgFname3[1024];

	int	w,h;
	double	mapX1, mapX2, mapY1, mapY2, zMin, zMax, z;

	float	 **ft2;
	char	buf[MAXBUFL];
	
	int	i,j;

	strcpy( imgFname2, imgFname1 );
	removeExtention2( imgFname2 );
	strcat( imgFname2, ".hdr");
	strcpy( imgFname3, 	pureFileName(imgFname2) );
	printf("surferAsciiGrid2ENVI %s -> %s\n", imgFname1, imgFname3 );


	imgf1 = fopen( imgFname1, "rt" );
	if( imgf1 == NULL ){
		fprintf(stderr, "surferAsciiGrid2ENVI: ASCII file open error: %s\n",imgFname1 );
		return 1;
	}

	if( fgets( buf, MAXBUFL, imgf1 ) == NULL ){
		fprintf(stderr,"surferAsciiGrid2ENVI: Not Surfer ASCII Grid File %s\n", imgFname1 );
		return 1;
	}
	if( strncmp( buf, "DSAA", 4 ) != 0 ){
		fprintf(stderr,"surferAsciiGrid2ENVI: Not Surfer ASCII Grid File %s\n", imgFname1 );
		return 1;
	}
	if(	fscanf( imgf1, "%d %d",&w, &h ) != 2 ){
		fprintf(stderr,"surferAsciiGrid2ENVI: width and height read error %s\n", imgFname1 );
		return 1;
	}
	if(	fscanf( imgf1, "%lf %lf",&mapX1, &mapX2 ) != 2 ){
		fprintf(stderr,"surferAsciiGrid2ENVI: mapx read error %s\n", imgFname1 );
		return 1;
	}
	if(	fscanf( imgf1, "%lf %lf",&mapY1, &mapY2 ) != 2 ){
		fprintf(stderr,"surferAsciiGrid2ENVI: mapy read error %s\n", imgFname1 );
		return 1;
	}
	if(	fscanf( imgf1, "%lf %lf",&zMin, &zMax ) != 2 ){
		fprintf(stderr,"surferAsciiGrid2ENVI: Z min max read error %s\n", imgFname1 );
		return 1;
	}

	img2=image_alloc( w, h, IMAGE_FLOAT, 1 );
	if( img2 == NULL ){
		fprintf(stderr,"surferAsciiGrid2ENVI: image alloc error\n");
		exit(1);
	}
	img2->geoRegistration.datum = IMAGE_DATUM_WGS84;
	img2->geoRegistration.projection = IMAGE_PROJECTION_GEOGRAPHIC;
	img2->geoRegistration.geoRegistrationX = 0;
	img2->geoRegistration.geoRegistrationY = 0;
	img2->geoRegistration.geoRegistrationMapX = mapX1;
	img2->geoRegistration.geoRegistrationMapY = mapY2;
	img2->geoRegistration.dx = ( mapX2-mapX1 ) / (w-1);
	img2->geoRegistration.dy = ( mapY2-mapY1 ) / (h-1);

	ft2=(float**) img2->data[0];
	for(i=h-1;i>0;i--){
		for(j=0;j<w;j++){
			fscanf( imgf1, "%lf", &z );
			ft2[i][j]=z;
		}
	}
	imgf2 = image_file_create( imgFname3, IMAGE_TRUNC, 0, img2 );
	if( imgf2 == NULL ){
		fprintf(stderr, "surferAsciiGrid2ENVI: image file create error: %s\n",imgFname2 );
		return 1;
	}
	image_file_close( imgf2 );
	image_destroy( img2 );
	fclose( imgf1 );

	return 0;
}

#define	EPS	1.0e-6


static int removeExtention( char *fname, char *extention )
{

	int	l;

	if( ( l = strlen( fname )) < 5 )
		return -1;
	if( strcmp( fname+l-4, extention ) )
		return -1;

	fname[ l-4 ] = 0;

	return 0;
}

static int removeExtention2( char *fname )
{

	int	l;

	l = strlen( fname );
	if( l > 4 && fname[l-4] == '.' ) {
		fname[l-4] = 0;
	} else if( l > 5 && fname[l-5] == '.' ){
		fname[l-5] = 0;
	} else {
		return -1;
	}
	return 0;
}

char *pureFileName( char *fname )
{
	char	*pt;
	pt = strchr(fname, '\\' );
	if( pt != NULL )
		return pt+1;
	else
		return	fname;
}