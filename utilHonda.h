#if !defined(UTIL_HONDA_H)
#define UTIL_HONDA_H

#if defined (WIN32) || defined(WIN64) || defined(_WIN32) || defined(_WIN64) 
#	if !defined (WINDOWS)
#		define WINDOWS
#	endif
#	if defined(LINUX)
#error	image.h: both WINDOWS and LINUX defined
#	endif
#endif 

#if !defined (WINDOWS)
#	if !defined (LINUX)
#		define LINUX
//#warning image.h: COMPILING FOR LINUX
#	endif
#endif

#if defined( UNIX ) || defined (LINUX)
//	#warning	utilHonda.h compiling for UNIX || LINUX
	#define	_stricmp(a,b)		strcasecmp(a,b)
	#define	_strnicmp(a,b,c)	strncasecmp(a,b,c)
	#define	DirectorySeparater	'/'
#else
//	#warning	utilHonda.h compiling for WINDOWS
	#define	strcasecmp(a,b)		_stricmp(a,b)
	#define	strncasecmp(a,b,c)	_strnicmp(a,b,c)
	#define	DirectorySeparater	'\\'
#endif

#if ! defined ( PI )
#define	PI	3.14159265358979323846264338327950288419716939937511
/* http://www1.coralnet.or.jp/kusuto/PI/pi-data.html */
/* http://pi2.cc.u-tokyo.ac.jp/index-j.html */
#endif

#define MIN(a,b)  (((a)<(b))?(a):(b))
#define MAX(a,b)  (((a)>(b))?(a):(b))

void perr_warning( const char *message, int code );
void perr_warning2( const char *component, const char *message, int code );
void perr_exit( const char *message, int code );
void perr_exit2( const char *component, const char *message, int code );
void	err_exit( const char *message, int code );
void err_exit2( const char *component, const char *message, int code );
void	erExit( const char *messeage, int code );
void err_warning2( const char *component, const char *message, int code );
void err_warning( const char *message, int code );

int	removeExtension( char *fname, const char *extension );
int	removeExtension2( char *fname );
char	*pureFileName( char *fname );
int	haveExtension ( const char *fname, const char *extension );
void	counterSet( int total );
void	counterDisp( int count );

char	*rmLeadingBlanks( char *str, const char *blanks );
char	*rmTrailingBlanks( char *str, const char *blanks );
char	*trim( char *str, const char *blanks );
char	*packBlanks( char *str, const char *blanks );
int	tokens( char *str, char **argv, char *separators, int margc );

typedef struct  {
		double	x,y,z;
} vect;

typedef struct  {
		double	x,y,z;
} point;

typedef struct {
	vect	l;
	point	origin;
	point	st, ed;
} line;

typedef struct {
	vect	normal;
	point	origin;
	double	d;	/*	-(nx*x0+ny*y0+nz*z0)	*/
} surface;

#define		MIN2SEC		60
#define		DEG2SEC		3600

vect *vectRotzLeft90( vect *x );
vect *vectRotzRight90( vect *x );
vect *vectCopy( vect *vdest, vect *vsrc );
vect *vectDup( vect *vsrc );
vect *vectConstruct ( vect *vdest, point *p );
vect *vectConstruct2 ( vect *vdest, point *p1, point *p2  );
vect *vectDiagonal( vect *v, vect *v1, vect *v2 );
double	vectAbsolute( vect *x );
vect *vectNormalize( vect *x  );
vect *vectFlip( vect *x );
vect *vectScale ( vect *x, double fact );

double	dotProd( vect *u, vect *v );
vect	*outProd( vect *u, vect *v, vect *p );

char *dmsStrBuf( double x, int ndigit, char *buf );
char *dmsStr( double x, int ndigit );

#if defined(WINDOWS)
#include<windows.h>
#define MSLEEP(MILLISEC)	Sleep((DWORD)(MILLISEC))
#else
#define _XOPEN_SOURCE 500     /* Or: #define _BSD_SOURCE */
#include <unistd.h>
#define MSLEEP(MILLISEC)	usleep((useconds_t)(MILLISEC*1000))
#endif
#define	VRML20HEADER	"#VRML V2.0 utf8"
#endif
