/*
usage: sleep seconds

LINUX has sleep shell command so no meaning to compiile for linux

K. Honda 2011

*/

#include<stdio.h>

#if defined(_WIN32) || defined(_WIN64) || defined(WIN32) || defined(WIN64)
#	if !defined(WINDOWS)
#		define WINDOWS
#	endif
#endif
#if !defined (WINDOWS)
#	if !defined(LINUX)
#		define(LINUX)
#	endif
#else
#	if defined(LINUX)
#		error LINUX and WINDOWS defined
#	endif
#endif

#if defined(WINDOWS)
#include<windows.h>
#define MSLEEP(MILLISEC)	Sleep((DWORD)(MILLISEC))
#else
#define _XOPEN_SOURCE 500     /* Or: #define _BSD_SOURCE */
#include <unistd.h>
#define MSLEEP(MILLISEC)	usleep((useconds_t)(MILLISEC*1000))
#endif

usage()
{
	printf( "usage: sleep seconds\n");
}
main( int argc, char **argv )
{

	unsigned long	millisec = 1000;

	if( argc != 2) {
		usage();
		return 255;
	}
	millisec = atol( argv[1] )*1000;

	MSLEEP( millisec );
	return 0;
}