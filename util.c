#include<stdio.h>
#include<string.h>
#include<math.h>
//#include"const_FLOAT.h"
#include"util.h"

// protypes in util.h
int	poly4IntersectRect(POLY4 poly4, RECT rect );
POLY4 newPoly4( POINT *vertex );
POLY4 newPoly4Xy( double x0, double y0, double x1, double y1, double x2, double y2, double x3, double y3 );
LINE_H newLine( POINT p1, POINT p2 );
RECT	newRect( POINT ll, POINT ur );
RECT	newBBox( POINT ll, POINT ur );
int	pointInRect( POINT pt, RECT rect );
int	pointInPoly4( POINT pt, POLY4  poly4 );
int	LineCrossLine( LINE_H l0, LINE_H l1 );
DMS deg2dms( double deg, int ssDecimal );

// protypes only in util.c
int	bboxIntersectBbox( BBOX bbox1, BBOX bbox2 );
LINE_H newLineXy( double x0, double y0, double x1, double y1 );
POLY4	rect2poly4( RECT rect );
BBOX genBBox( POINT *vertex, int n );

#define	PROG	"poly4rect.a"
#if defined(MAIN)

int main()
{
	POINT	ll, ur;
	RECT	rect;
	int	i;

	POINT	vertex[4];
	POLY4	poly4;

	printf("Input rect ll x,y\n");
	if( scanf("%lf%lf",&ll.x, &ll.y ) !=2 ){
		fprintf(stderr, PROG ": ll read error\n");
		return 1;
	}
	printf("Input rect ur x,y\n");
	if( scanf("%lf%lf",&ur.x, &ur.y ) != 2 ){
		fprintf(stderr, PROG ": ur read error\n");
		return 1;
	}
	rect = newRect( ll, ur );

	printf("Input 4 vertex of poly4 counter-clock wise\n");
	for(i=0;i<4;i++){
		if( scanf("%lf%lf",&vertex[i].x, &vertex[i].y ) !=2 ){
			fprintf(stderr, PROG ": vertex[%d] read error\n",i);
			return 1;
		}
	}

	poly4 = newPoly4( vertex );

	printf("result of poly4IntersecRect is %d\n", poly4IntersectRect( poly4, rect ) );
}
#endif

// Convert deg into ddd:mm:ss.ss

#define	DMS_NONE	0
#define	DMS_LATITUDE	1
#define	DMS_LONGITUDE	2
/*!
	@brief	deg2dms() generate a DMS object
	@parm	deg: degree value
	ssDecimal: decimal point number for second
*/
DMS deg2dms( double deg, int ssDecimal )
{
	DMS	dms;
	double	degabs;
	int	signChar;
	int	i;
	char	*fmtddmm = (char*)"%c%3d:%2.2d:%.";
	char	fmt[32]; 

	const double 	EPS = 	(1./3600./30./1000.);

	if( fabs(deg) < EPS )
		deg = 0.;

	dms.deg = deg;
	degabs = fabs(deg);

	if( deg < 0 ) {
		dms.sign = -1;
		signChar = '-';
	} else if ( deg == 0. ){
		dms.sign = 1;
		signChar = ' ';
	} else {
		dms.sign =  1;
		signChar = '+';
	}

	dms.dd = (int) ( degabs + EPS);
	dms.mm = (int) ( (degabs - dms.dd)*60.+EPS*60);
	dms.ss = ((degabs-dms.dd)*60.- dms.mm)*60.;
	if( dms.ss < 0 ) dms.ss = 0.;

//	sprintf(fmt,"%s%d%s", fmtddmm,ssDecimal,"lf%c");
	sprintf(fmt,"%s%d%s", fmtddmm,ssDecimal,"lf");
	sprintf( dms.string, fmt, signChar, dms.dd, dms.mm, dms.ss);

//	0 padding
	for(i=1; i<strlen(dms.string); i++){
		if (dms.string[i] == ' ' )
			dms.string[i] = '0';
	}
	return dms;
}

RECT	newRect( POINT ll, POINT ur )
{
	RECT	rect;

	rect.ll = ll;
	rect.ur = ur;
	rect.dx = ur.x - ll.x;
	rect.dy = ur.y - ll.y;

	if( rect.dx < 0 ){
		rect.ll.x = ur.x;
		rect.ur.x = ll.x;
		rect.dx = - rect.dx;
	}
	if( rect.dy < 0 ){
		rect.ll.y = ur.y;
		rect.ur.y = ll.y;
		rect.dy = - rect.dy;
	}

	rect.vertex[0].x = ll.x;
	rect.vertex[0].y = ll.y;
	rect.vertex[1].x = ur.x;
	rect.vertex[1].y = ll.y;
	rect.vertex[2].x = ur.x;
	rect.vertex[2].y = ur.y;
	rect.vertex[3].x = ll.x;
	rect.vertex[3].y = ur.y;

	rect.sign = 1;

	return rect;
}
int	poly4IntersectRect(POLY4 poly4, RECT rect )
{
	BBOX	poly4Bbox;
	BBOX	rectBbox;
	LINE_H	pLine[4], rLine[4];
	POLY4	poly4rect;

	int	i, j;


	poly4Bbox = genBBox( poly4.vertex, 4 );
	rectBbox = rect;

//	if bounding box is not intersecting then return 0
//	printf("bbox of rect  is %lf %lf %lf %lf\n", rectBbox.ll.x, rectBbox.ll.y, rectBbox.ur.x, rectBbox.ur.y);
//	printf("bbox of poly4 is %lf %lf %lf %lf\n", poly4Bbox.ll.x, poly4Bbox.ll.y, poly4Bbox.ur.x, poly4Bbox.ur.y);
	if( bboxIntersectBbox( poly4Bbox, rectBbox ) == 0 ) {
//		printf("boundig boxes are not intersecting\n");
		return 0;
	}

//	if poly4 point is in rect, then return 1
	for(i=0;i<4;i++)
		if( pointInRect( poly4.vertex[i], rect ) )
			return 1;

//	if rect point is in poly4, then return 1
	poly4rect = rect2poly4( rect );
	for(i=0;i<4;i++){
		if( pointInPoly4( poly4rect.vertex[i] , poly4 ) != 0 )
			return 1;
	}

	pLine[0] = newLine( poly4.vertex[0], poly4.vertex[1] );
	pLine[1] = newLine( poly4.vertex[1], poly4.vertex[2] );
	pLine[2] = newLine( poly4.vertex[2], poly4.vertex[3] );
	pLine[3] = newLine( poly4.vertex[3], poly4.vertex[0] );

	rLine[0] = newLineXy( rect.ll.x, rect.ll.y, rect.ur.x, rect.ll.y );
	rLine[1] = newLineXy( rect.ur.x, rect.ll.y, rect.ur.x, rect.ur.y );
	rLine[2] = newLineXy( rect.ur.x, rect.ur.y, rect.ll.x, rect.ur.y );
	rLine[3] = newLineXy( rect.ll.x, rect.ur.y, rect.ll.x, rect.ll.y );

//	check if line and line corss
	for(i=0;i<4;i++){
		for(j=0;j<4;j++){
			if( LineCrossLine( pLine[i], rLine[j] ) )
				return 1;
		}
	}
	return 0;
}
// Judge if a point pt is inside of poly4
int	pointInPoly4( POINT pt, POLY4  poly4 )
{
	int	nvert = 4;
	int	i,j,c = FALSE;
	double	x, y;

	x = pt.x;
	y = pt.y;
	for (i=0, j=nvert-1;i<nvert; j=i++) {
        	if (((poly4.vertex[i].y > y) != (poly4.vertex[j].y > y)) && (x < (poly4.vertex[j].x - poly4.vertex[i].x) * (y - poly4.vertex[i].y) / (poly4.vertex[j].y - poly4.vertex[i].y) + poly4.vertex[i].x))
			c = !c;
	}
	return c;
/*
public static function pnpoly(nvert: int, vertx: Array, verty: Array, x: Number, y: Number): Boolean
{
    var i: int, j: int;
    var c: Boolean = false;
    for (i = 0, j = nvert - 1; i < nvert; j = i++)
    {
        if (((verty[i] > y) != (verty[j] > y)) && (x < (vertx[j] - vertx[i]) * (y - verty[i]) / (verty[j] - verty[i]) + vertx[i]))
            c = !c;
    }
    return c;
*/
}


// Judge if a point is in RECT
int	pointInRect( POINT pt, RECT rect )
{
	if( pt.x >= rect.ll.x && pt.x <= rect.ur.x && pt.y >= rect.ll.y && pt.y <= rect.ur.y )
		return 1;
	else
		return 0;
}

// Judge if two lines cross or not
int	LineCrossLine( LINE_H l0, LINE_H l1 )
{
	double x1, x2, x3, x4;
	double y1, y2, y3, y4;

	x1 = l0.p0.x;
	y1 = l0.p0.y;
	x2 = l0.p1.x;
	y2 = l0.p1.y;
	x3 = l1.p0.x;
	y3 = l1.p0.y;
	x4 = l1.p1.x;
	y4 = l1.p1.y;

	if( ( (x2-x1)*(y3-y1) - (y2-y1)*(x3-x1) ) * 
	    ( (x2-x1)*(y4-y1) - (y2-y1)*(x4-x1) )  > 0 )
		return 0;
	if( ( (x4-x3)*(y1-y3) - (y4-y3)*(x1-x3) ) * 
	    ( (x4-x3)*(y2-y3) - (y4-y3)*(x2-x3) )  > 0 )
		return 0;

	return 1;
}

//	if bounding box and bounding box intersects
int	bboxIntersectBbox( BBOX bbox1, BBOX bbox2 )
{
	if( bbox1.ur.x < bbox2.ll.x ||
	    bbox1.ll.x > bbox2.ur.x ||
	    bbox1.ll.y > bbox2.ur.y ||
	    bbox1.ur.y < bbox2.ll.y  )
		return 0;
	return 1;
}

POLY4	rect2poly4( RECT rect )
{
	return newPoly4Xy( rect.ll.x, rect.ll.y, rect.ur.x, rect.ll.y, rect.ur.x, rect.ur.y, rect.ll.x, rect.ur.y );
}

POLY4 newPoly4( POINT *vertex )
{
	POLY4	poly4;
	int	i;
	for(i=0;i<4;i++){
		poly4.vertex[i].x = vertex[i].x;
		poly4.vertex[i].y = vertex[i].y;
	}
	return poly4;
}
POLY4 newPoly4Xy( double x0, double y0, double x1, double y1, double x2, double y2, double x3, double y3 )
{
	POLY4	poly4;
	poly4.vertex[0].x = x0;
	poly4.vertex[0].y = y0;
	poly4.vertex[1].x = x1;
	poly4.vertex[1].y = y1;
	poly4.vertex[2].x = x2;
	poly4.vertex[2].y = y2;
	poly4.vertex[3].x = x3;
	poly4.vertex[3].y = y3;
	return poly4;
}

BBOX genBBox( POINT *vertex, int n )
{
	BBOX	bbox;
	int	i;

	double	xmin, xmax, ymin, ymax;

	xmax = xmin = vertex[0].x;
	ymax = ymin = vertex[0].y;
	for(i=1;i<n;i++){
		if( xmin > vertex[i].x ) xmin = vertex[i].x;
		if( xmax < vertex[i].x ) xmax = vertex[i].x;
		if( ymin > vertex[i].y ) ymin = vertex[i].y;
		if( ymax < vertex[i].y ) ymax = vertex[i].y;
	}
	bbox.ll.x = xmin;
	bbox.ll.y = ymin;
	bbox.ur.x = xmax;
	bbox.ur.y = ymax;
	return bbox;
}

LINE_H newLine( POINT p1, POINT p2 )
{
	return newLineXy( p1.x, p1.y, p2.x, p2.y );	
}

LINE_H newLineXy( double x0, double y0, double x1, double y1 )
{
	LINE_H	line;
	line.p0.x = x0;
	line.p0.y = y0;
	line.p1.x = x1;
	line.p1.y = y1;
	return line;
}

#include<sys/time.h>
static struct timeval start, end;
static FILE *timerFp;
void timer_init( FILE *fp ){
        gettimeofday(&start, NULL);
	if( fp == NULL )
		timerFp = stdout;
	else
		timerFp = fp;
}
void timer_diff(const char *desc){
        gettimeofday(&end, NULL);
        fprintf( timerFp, "%-30s \t[%lf seconds]\n", desc, (end.tv_sec+(double)end.tv_usec/1000000.0) - (start.tv_sec+(double)start.tv_usec/1000000.0));
        start = end;
}

