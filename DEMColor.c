// ColTbl.txt globdem_-0011_0160_-0012_0072.hdr to create Asian Highway color October 2003

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>

#include"image.h"
#include"imgio.h"

#include"utilHonda.h"

#define	MSLICE	1000
double	flim[MSLICE];
int		tr[MSLICE], tg[MSLICE], tb[MSLICE];
int		nSlice;

#define	MAINNAME	"colorSlice"

typedef struct	COLORTABLETAG {
	int	n;
	int	continuous;
	int	hsi;
	double	*v;
	int		*r, *g, *b;
	double	*h, *s, *i;
} COLORTABLE;


void usage();
int colorSlice( char *imgFname1, COLORTABLE *, int rNull, int gNull, int bNull );
static int removeExtention2( char *fname );
void	getRGB( double	v, int *r, int *g, int *b, COLORTABLE *colorTable );
char *pureFileName( char *fname );
IMAGE	*readMask( char *fName );
COLORTABLE *readColTbl( char *fname );
void colorTable2ENVIDSR( COLORTABLE *colorTable, char *fname );
COLORTABLE	*colorTableExpand ( COLORTABLE *colorTable, int elev1, int elev2, int interval );
void	colorTablePrint( COLORTABLE * colorTable );

main( int argc, char *argv[])
{
	char	*colTblFname, *imgFname;
	int		rNull, gNull, bNull;
	COLORTABLE	*colorTable, *exColorTable;
	int	elev1, elev2;

	if( argc < 3 ){
		usage();
		exit(1);
	}
	colTblFname=argv[1];
	colorTable = readColTbl( colTblFname );

	imgFname = argv[2];

	rNull = gNull = bNull = 255;

	exColorTable = colorTableExpand( colorTable, -250, 9000, 1 );
	colorTablePrint( exColorTable );
	colorTable2ENVIDSR( exColorTable, colTblFname );

	colorSlice( imgFname, colorTable, rNull, gNull, bNull );

	return 0;
}


void usage()
{
	fprintf(stderr, "DEMColor colorTable DEMfile\n");
}

COLORTABLE	*colorTableAlloc( int n )
{
	COLORTABLE	*colorTable;

	colorTable = malloc( sizeof( COLORTABLE ) );
	colorTable->n = n;
	colorTable->continuous = FALSE;
	colorTable->hsi = FALSE;
	colorTable->v = malloc( sizeof( double )*n );
	colorTable->r = malloc( sizeof( int )*n );
	colorTable->g = malloc( sizeof( int )*n );
	colorTable->b = malloc( sizeof( int )*n );
	colorTable->h = malloc( sizeof( double )*n );
	colorTable->s = malloc( sizeof( double )*n );
	colorTable->i = malloc( sizeof( double )*n );

	if( colorTable    == NULL ||
		colorTable->v == NULL ||
		colorTable->r == NULL ||
		colorTable->g == NULL ||
		colorTable->b == NULL ||
		colorTable->h == NULL ||
		colorTable->s == NULL ||
		colorTable->i == NULL  ) {
		fprintf(stderr, "colorTableAlloc: not enough core %d", n );
		exit(1);
	}
	return colorTable;
}

COLORTABLE	*colorTableCopy( COLORTABLE *colorTable1, int nslice )
{
	COLORTABLE	*colorTable2;
	int	i;

	colorTable2 = colorTableAlloc( nslice );
	colorTable2->hsi = colorTable1->hsi;
	colorTable2->continuous = colorTable1->continuous;
	for(i=0; i<nslice; i++){
		colorTable2->v = colorTable1->v;
		colorTable2->r = colorTable1->r;
		colorTable2->g = colorTable1->g;
		colorTable2->b = colorTable1->b;
		colorTable2->h = colorTable1->h;
		colorTable2->s = colorTable1->s;
		colorTable2->i = colorTable1->i;
	}
	return colorTable2;
}

void	colorTableSetRGBHSI( COLORTABLE *colorTable )
{
	int	i;

	if( colorTable->hsi ){
		double	*rgb, hsi[3];
		for(i=0; i<colorTable->n; i++ ){
			hsi[0] = colorTable->h[i]/180.*PI;
			hsi[1] = colorTable->s[i]/100.;
			hsi[2] = colorTable->i[i]/100.;
			rgb = to_rgb( hsi );
			colorTable->r[i] = rgb[0]*255;
			colorTable->g[i] = rgb[1]*255;
			colorTable->b[i] = rgb[2]*255;
		}
	} else {
		double	*hsi, rgb[3];
		for(i=0; i<colorTable->n; i++ ){
			rgb[0] = colorTable->r[i]/255.;
			rgb[1] = colorTable->g[i]/255.;
			rgb[2] = colorTable->b[i]/255.;
			hsi = to_hsi( rgb );
			colorTable->h[i] = hsi[0]/PI*180.;
			colorTable->s[i] = hsi[1]*100.;
			colorTable->i[i] = hsi[2]*100.;
		}
	}
}

COLORTABLE	*colorTableExpand ( COLORTABLE *colorTable, int elev1, int elev2, int interval )
{
	COLORTABLE	*exColorTable;
	int	n, elev, i;
	int	r,g,b;

	n = (elev2-elev1)/interval;
	exColorTable = colorTableAlloc( n );
	exColorTable->hsi = FALSE;
	exColorTable->continuous = FALSE;

	for(i=0, elev=elev1; i<n; i++, elev+=interval ){
		getRGB( (double) elev, &r, &g, &b, colorTable );
		exColorTable->v[i] = elev;
		exColorTable->r[i] = r;
		exColorTable->g[i] = g;
		exColorTable->b[i] = b;
	}

		colorTableSetRGBHSI ( exColorTable );

	return exColorTable;
}

void colorTable2ENVIDSR( COLORTABLE *colorTable, char *fName )
{
	char	dsrFName[1024];
	FILE	*fp;
	int	i;
	int	elev1, elev2;

	elev1 = -10000;

	strcpy( dsrFName, fName );
	removeExtention2( dsrFName );
	strcat( dsrFName, ".dsr");

	fp=fopen( dsrFName, "wt" );

	printf("colorTable2ENVIDSR: %s\n", dsrFName );
	fprintf(fp, "ENVI Density Slice Range File\n");
	for(i=0; i<colorTable->n; i++ ){
		fprintf(fp, "%6d %6d %5d %5d %5d\n",elev1, (int) colorTable->v[i],
			colorTable->r[i], colorTable->g[i], colorTable->b[i] );
		elev1 = (int) colorTable->v[i] +1;
	}
	fclose( fp );
	printf("colorTable2ENVIDSR: done\n" );
}

void	colorTablePrint( COLORTABLE * colorTable )
{
	int	n, i;
	n = colorTable->n;
	printf( "ColorTable\n");
	printf( " n  = %d", colorTable->n );
	printf( " hsi= %d", colorTable->hsi );
	printf( " continuous= %d\n", colorTable->continuous );
	for(i=0; i<n; i++ ){
		printf("%5d %8.1lf %4d %4d %4d %8.2lf %5.1lf %5.1lf\n", i,
			colorTable->v[i],
			colorTable->r[i],
			colorTable->g[i],
			colorTable->b[i],
			colorTable->h[i],
			colorTable->s[i],
			colorTable->i[i] );
	}
}

COLORTABLE *readColTbl( char *fname )
{
#define	BUFL	1024

	FILE	*fp;
	char	buf[BUFL];

	COLORTABLE	*colorTable1, *colorTable2;

	colorTable1 = colorTableAlloc( MSLICE );

	fp=fopen( fname, "r" );
	if( fp == NULL ){
		fprintf( stderr, "colorSlice: color table file open error: %s\n", fname );
		exit(1);
	}
	
	nSlice=0;
	printf( "colorSlice: color table data: %s\n", fname);
	colorTable1->hsi = FALSE;
	while( fgets( buf, BUFL, fp ) != NULL ){
		printf("%s", buf );
		if( buf[0] == '#' )
			continue;
		
		if( strnicmp(buf, "HSI", 3 ) == 0 ){
			colorTable1->hsi = TRUE;
			continue;
		}
		if( strnicmp(buf, "RGB", 3 ) == 0 ){
			colorTable1->hsi = FALSE;
			continue;
		}
		if( strnicmp(buf, "CONTINUOUS", 10 ) == 0 ){
			colorTable1->continuous = TRUE;
			continue;
		}
		if( strnicmp(buf, "STEP", 4 ) == 0 ){
			colorTable1->continuous = FALSE;
			continue;
		}

		if( ! colorTable1->hsi ) {
			if( sscanf( buf, "%lf %d %d %d",
				&colorTable1->v[nSlice],
				&colorTable1->r[nSlice],
				&colorTable1->g[nSlice], 
				&colorTable1->b[nSlice] ) != 4 ){
				fprintf( stderr, "colorSlice: RGB color table read error: %s\n", fname );
				exit(1);
			}
		} else {
			if( sscanf( buf, "%lf %lf %lf %lf",
				&colorTable1->v[nSlice],
				&colorTable1->h[nSlice],
				&colorTable1->s[nSlice], 
				&colorTable1->i[nSlice] ) != 4 ){
				fprintf( stderr, "colorSlice: HSI color table read error: %s\n", fname );
				exit(1);
			}
		}

		nSlice++;
		if( nSlice > MSLICE ){
			fprintf(stderr, "colorSlice: too many slices\n");
			exit(1);
		}
	}
	printf("readColTbl: %d\n", nSlice);
	fclose( fp );

	colorTable2 = colorTableCopy( colorTable1, nSlice );
	colorTableSetRGBHSI( colorTable2 );


	free( colorTable1 );
	colorTablePrint( colorTable2 );
	return colorTable2;
}


int colorSlice( char *imgFname1, COLORTABLE *colorTable, int rNull, int gNull, int bNull )
{
	IMAGE_FILE	*imgf1, *imgf2;
	IMAGE				*img2;
	IMAGE				*bufImg1, *bufImg2, *bufImg1_3;
	char		imgFname2[1024];
	char		imgFname3[1024];

	int	w,h;
	int	r,g,b;

	u_char	**msk;
	
	int	i,j;

	strcpy( imgFname2, imgFname1 );
	removeExtention2( imgFname2 );
	strcat( imgFname2, "_col.hdr");
	strcpy( imgFname3, 	pureFileName(imgFname2) );
	printf("colorSlice %s -> %s\n", imgFname1, imgFname3 );

	imgf1=image_file_open( imgFname1, IMAGE_RDONLY, IMAGE_NO_BUF );
	if( imgf1 == NULL ){
		fprintf(stderr, "colorSlice: image file open error: %s\n",imgFname1 );
		return 1;
	}

	w=imgf1->w;
	h=imgf1->h;
	msk = NULL;

	img2=image_skelton( w, h, IMAGE_CHAR, 3 );
	if( img2 == NULL ){
		fprintf(stderr,"colorSlice: image alloc error\n");
		exit(1);
	}
	img2->geoRegistration = imgf1->geoRegistration;

	imgf2 = image_file_create( imgFname3, IMAGE_TRUNC, 0, img2 );
	if( imgf2 == NULL ){
		fprintf(stderr, "colorSlice: image file create error: %s\n",imgFname2 );
		return 1;
	}

	bufImg1 = image_alloc( w, 1, imgf1->data_type, 1 );
	bufImg2 = image_alloc( w, 1, IMAGE_CHAR, 3 );

	bufImg1_3 = image_alloc( w, 3, imgf1->data_type, 1 );
	for(i=0; i<h; i++ ){
		printf("colorSlize i= %d\n",i);
		image_file_read( imgf1, 0, i, w, 1, 0, bufImg1->data[0] );

		if( imgf1->data_type == IMAGE_SHORT ){
			short	*st;
			u_char	*rp, *gp, *bp;

			st = (short*) bufImg1->data[0][0];
			rp = bufImg2->data[0][0];
			gp = bufImg2->data[1][0];
			bp = bufImg2->data[2][0];
			for(j=0;j<w;j++){
				getRGB( (double) st[j], &r, &g, &b, colorTable );
/*				if( msk == NULL || msk[i][j] != 0 ){
					getRGB( (double) st[j], &r, &g, &b, colorTable );
				} else {
					r = rNull;
					g = gNull;
					b = bNull;
				}
*/
				*rp=r;
				*gp=g;
				*bp=b;

				rp++; gp++; bp++;
			}
		} else {
			fprintf(stderr, "this data type is not being supported %d\n",imgf1->data_type );
			exit(1);
		}
		image_file_write( imgf2, 0, i, w, 1, 0, bufImg2->data[0] );
		image_file_write( imgf2, 0, i, w, 1, 1, bufImg2->data[1] );
		image_file_write( imgf2, 0, i, w, 1, 2, bufImg2->data[2] );
	}
	image_file_close( imgf2 );
	image_file_close( imgf1 );
	image_destroy( img2 );

	return 0;
}

int colorSlice2( char *imgFname1, COLORTABLE *colorTable, int rNull, int gNull, int bNull )
{
	IMAGE_FILE	*imgf1, *imgf2;
	IMAGE				*img2;
	IMAGE				*bufImg1, *bufImg2, *bufImg1_3;
	char		imgFname2[1024];
	char		imgFname3[1024];

	int	w,h;
	int	r,g,b;

	u_char	**msk;
	
	int	i,j;

	strcpy( imgFname2, imgFname1 );
	removeExtention2( imgFname2 );
	strcat( imgFname2, "_col.tif");
	strcpy( imgFname3, 	pureFileName(imgFname2) );
	printf("colorSlice %s -> %s\n", imgFname1, imgFname3 );

	imgf1=image_file_open( imgFname1, IMAGE_RDONLY, IMAGE_NO_BUF );
	if( imgf1 == NULL ){
		fprintf(stderr, "colorSlice: image file open error: %s\n",imgFname1 );
		return 1;
	}

	w=imgf1->w;
	h=imgf1->h;
	msk = NULL;

//	img2=image_alloc( w, h, IMAGE_CHAR, 3 );
	img2=image_skelton( w, h, IMAGE_CHAR, 3 );
	if( img2 == NULL ){
		fprintf(stderr,"colorSlice: image alloc error\n");
		exit(1);
	}
	img2->geoRegistration = imgf1->geoRegistration;

	imgf2 = image_file_create( imgFname3, IMAGE_TRUNC, 0, img2 );
	if( imgf2 == NULL ){
		fprintf(stderr, "colorSlice: image file create error: %s\n",imgFname2 );
		return 1;
	}

	bufImg1 = image_alloc( w, 1, imgf1->data_type, 1 );
	bufImg2 = image_alloc( w, 1, IMAGE_CHAR, 3 );

	bufImg1_3 = image_alloc( w, 3, imgf1->data_type, 1 );
	image_file_read( imgf1, 0, 0, w, 1, 0, bufImg1->data[0] );
	memcpy( bufImg1_3->data[0][0], bufImg1->data[0][0], bufImg1->md_linebytes );
	memcpy( bufImg1_3->data[0][1], bufImg1->data[0][0], bufImg1->md_linebytes );
	memcpy( bufImg1_3->data[0][2], bufImg1->data[0][0], bufImg1->md_linebytes );
	for(i=0; i<h; i++ ){
		printf("colorSlice i= %d\n",i);
		if( i < h-1 )
			image_file_read( imgf1, 0, i+1, w, 1, 0, bufImg1->data[0] );
		memcpy( bufImg1_3->data[0][0], bufImg1_3->data[0][1], bufImg1->md_linebytes );
		memcpy( bufImg1_3->data[0][1], bufImg1_3->data[0][2], bufImg1->md_linebytes );
		memcpy( bufImg1_3->data[0][2], bufImg1->data[0][0], bufImg1->md_linebytes );

		if( imgf1->data_type == IMAGE_SHORT ){
			short	*st;
			u_char	*rp, *gp, *bp;

			st = (short*) bufImg1_3->data[0][1];
			rp = bufImg2->data[0][0];
			gp = bufImg2->data[1][0];
			bp = bufImg2->data[2][0];
			for(j=0;j<w;j++){
//		printf("colorSlize j= %d/%d\n",j, w);
				getRGB( (double) st[j], &r, &g, &b, colorTable );
//		printf("colorSize: back from RGB %d: %d %d %d\n",st[j], r,g,b);
/*				if( msk == NULL || msk[i][j] != 0 ){
					getRGB( (double) st[j], &r, &g, &b, colorTable );
				} else {
					r = rNull;
					g = gNull;
					b = bNull;
				}
*/
				*rp=r;
				*gp=g;
				*bp=b;

				rp++; gp++; bp++;
			}
		} else {
			fprintf(stderr, "this data type is not being supported %d\n",imgf1->data_type );
			exit(1);
		}
		image_file_write( imgf2, 0, i, w, 1, 0, bufImg2->data[0] );
		image_file_write( imgf2, 0, i, w, 1, 1, bufImg2->data[1] );
		image_file_write( imgf2, 0, i, w, 1, 2, bufImg2->data[2] );
//		memcpy( bufImg1_3->data[0][0], bufImg1->data[0][0], bufImg1->md_linebytes );
	}
	image_file_close( imgf2 );
	image_file_close( imgf1 );
	image_destroy( img2 );

	return 0;
}
#define	EPS	1.0e-6

void	getRGB( double	v, int *r, int *g, int *b, COLORTABLE *colorTable )
{
	int	i;

	int	nSlice;
	double	*rgb;
	double	hsi[3];
	double	a1, a2;

	nSlice = colorTable->n;
	if( ! colorTable->continuous ) {
		*r = colorTable->r[nSlice-1];
		*g = colorTable->g[nSlice-1];
		*b = colorTable->b[nSlice-1];
		for(i=0;i<nSlice;i++){
			if( v <= colorTable->v[i] ) {
				*r = colorTable->r[i];
				*g = colorTable->g[i];
				*b = colorTable->b[i];
				break;
			}
		}
	} else {
		*r = colorTable->r[nSlice-1];
		*g = colorTable->g[nSlice-1];
		*b = colorTable->b[nSlice-1];
		for(i=0;i<nSlice;i++){
			if( v <= colorTable->v[i] ) {
				if( i == 0 || v == colorTable->v[i]) {
					*r = colorTable->r[i];
					*g = colorTable->g[i];
					*b = colorTable->b[i];
				} else {
					a1 = ( v-colorTable->v[i-1] )/( colorTable->v[i] - colorTable->v[i-1] );
					a2 = 1-a1;
					if( ! colorTable->hsi ){
						*r = a2*colorTable->r[i-1] + a1*colorTable->r[i];
						*g = a2*colorTable->g[i-1] + a1*colorTable->g[i];
						*b = a2*colorTable->b[i-1] + a1*colorTable->b[i];
					} else {
						hsi[0] = (a2*colorTable->h[i-1] + a1*colorTable->h[i])/180.*PI;
						hsi[1] = (a2*colorTable->s[i-1] + a1*colorTable->s[i])/100.;
						hsi[2] = (a2*colorTable->i[i-1] + a1*colorTable->i[i])/100.;
						rgb = to_rgb(hsi);
						*r = rgb[0]*255;
						*g = rgb[1]*255;
						*b = rgb[2]*255;
					}
				}
				break;
			}
		}	
	}
//	if( v > 0.0001 ) 
//	printf("getRGB: %lf %d %d %d %d %d\n", v, i, nSlice,*r, *g, *b );
	return;
}

static int removeExtention( char *fname, char *extention )
{

	int	l;

	if( ( l = strlen( fname )) < 5 )
		return -1;
	if( strcmp( fname+l-4, extention ) )
		return -1;

	fname[ l-4 ] = 0;

	return 0;
}

static int removeExtention2( char *fname )
{

	int	l;

	l = strlen( fname );
	if( l > 4 && fname[l-4] == '.' ) {
		fname[l-4] = 0;
	} else if( l > 5 && fname[l-5] == '.' ){
		fname[l-5] = 0;
	} else {
		return -1;
	}
	return 0;
}

char *pureFileName( char *fname )
{
	char	*pt;
	pt = strchr(fname, '\\' );
	if( pt != NULL )
		return pt+1;
	else
		return	fname;
}