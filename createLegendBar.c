#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>
#include<math.h>

#include"image.h"
#include"imgio.h"

#include"utilHonda.h"

#define	MSLICE	1000
double	flim[MSLICE];
int		tr[MSLICE], tg[MSLICE], tb[MSLICE];
int		nSlice;

#define	MAINNAME	"createLegendBar"

typedef struct	COLORTABLETAG {
	int	n;
	int	continuous;
	int	hsi;
	double	*v;
	int		*r, *g, *b;
	double	*h, *s, *i;
} COLORTABLE;


void usage();
int colorSlice( char *imgFname1, COLORTABLE *, int rNull, int gNull, int bNull );
static int removeExtention2( char *fname );
void	getRGB( double	v, int *r, int *g, int *b, COLORTABLE *colorTable );
char *pureFileName( char *fname );
IMAGE	*readMask( char *fName );
COLORTABLE *readColTbl( char *fname );
void colorTable2ENVIDSR( COLORTABLE *colorTable, char *fname );
COLORTABLE	*colorTableExpand ( COLORTABLE *colorTable, int elev1, int elev2, int interval );
void	colorTablePrint( COLORTABLE * colorTable );

#define	DTTYPE	IMAGE_SHORT
typedef short dttype;

main( int argc, char *argv[])
{
	char	*legendBarFname;
	IMAGE_FILE	*imgf;
	IMAGE		*img;
	dttype	**pix;

	int		w, h;
	double	vmin, vmax;

	int	i,j;

	if( argc < 6 ){
		usage();
		exit(1);
	}
	legendBarFname=argv[1];
	w = atoi( argv[2] );
	h = atoi( argv[3] );
	vmin = atof( argv[4] );
	vmax = atof( argv[5] );

	img = image_alloc( w, h, DTTYPE, 1 );
	pix = (dttype ** ) img->data[0];

	{
	double	v;
	double	ii, iimin, iimax;
	iimax = 1.0;
	iimin = 0.0;
	for( i=0; i< h; i++ ){
		ii = pow( ( h-1-i )/(double)(h-1), 2. );
		v = ( vmax - vmin )/(iimax-iimin) * ( ii-iimin ) + vmin;
		for( j=0; j<w; j++ ){
			pix[i][j] = v;
		}
	}
	}

	imgf = image_file_create( legendBarFname, IMAGE_TRUNC, 0, img );

	image_file_close ( imgf );

	return 0;
}


void usage()
{
	fprintf(stderr, "createLegendBar filename w h v_min v_max\n");
}
