all: libutilHonda.a fileDump.a rmcrlf.a space2newline.a

clean:
	rm *.o

install: libutilHonda.a fileDump.a rmcrlf.a download download.py utilpy3.py
	cp -p libutilHonda.a /usr/local/lib
	cp -p utilHonda.h /usr/local/include
	cp -p fileDump.a /usr/local/bin
	cp -p rmcrlf.a /usr/local/bin
	cp -p space2newline.a /usr/local/bin
	cp -p download /usr/local/bin
	cp -p download.py /usr/local/lib/python2.7
	cp -p utilpy3.py /usr/local/lib/python2.7

utilHonda.o: utilHonda.c utilHonda.h
	gcc -c -D LINUX -Wall utilHonda.c

util.o: util.c util.h
	gcc -c -D LINUX -Wall util.c

libutilHonda.a: utilHonda.o utilHonda.h util.o
	ar rc libutilHonda.a utilHonda.o util.o
	ranlib libutilHonda.a
fileDump.a: fileDump.c utilHonda.h
	gcc -DLINUX fileDump.c -o fileDump.a
rmcrlf.a: rmcrlf.c utilHonda.h
	gcc -DLINUX rmcrlf.c -o rmcrlf.a
space2newline.a: space2newline.c utilHonda.h
	gcc -DLINUX space2newline.c -o space2newline.a
