import urllib2
import shutil
import urlparse
import os
import argparse

from utilpy3 import *

# http://stackoverflow.com/questions/862173/how-to-download-a-file-using-python-in-a-smarter-way/863017

def download(url, fileName=None):

	def getFileName(url,openUrl):
		if 'Content-Disposition' in openUrl.info():
		# If the response has Content-Disposition, try to get filename from it
			cd = dict(map(
				lambda x: x.strip().split('=') if '=' in x else (x.strip(),''),
				openUrl.info()['Content-Disposition'].split(';')))
			if 'filename' in cd:
				filename = cd['filename'].strip("\"'")
				if filename: return filename
		# if no filename was found above, parse it out of the final URL.
		return os.path.basename(urlparse.urlsplit(openUrl.url)[2])

	firefox_request_headers = {
	"Accept-Language": "en-US,en;q=0.5",
	"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:51.0) Gecko/20100101 Firefox/51.0",
	"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
	"Accept-Language": "ja,en-US;q=0.7,en;q=0.3",
	"Accept-Encoding": "gzip, deflate",
	"Connection": "keep-alive" 
	}

#	print 'download(): url = %s' % url
	try:
		request = urllib2.Request(url, headers=firefox_request_headers ) 
		r = urllib2.urlopen( request )
	except:
		eprint( 'download(): urlopen error for %s' % url )

	try:
		fileName = fileName or getFileName(url,r)
		print 'downloading [%s] -> [%s]'%( url,fileName)
		with open(fileName, 'wb') as f:
			shutil.copyfileobj(r,f)
	finally:
		r.close()

	return fileName

def main():

#	download testing
#	download( 'http://meteocrop.dc.affrc.go.jp/real/download.php?kind=5&id=34111' )
#	download( 'http://www.hondalab.net/index.html' )
#	return

	parser = argparse.ArgumentParser()
	parser.add_argument("--dstdir", nargs = 1, type=str, help="directoy to download ( optional )")
	parser.add_argument("urls", metavar='url', type=str, nargs='+', help="url of the targe file to download")

	args = parser.parse_args()
	urls = args.urls
#	print 'url=', urls
	if( args.dstdir == None ):
		dir = '.'
#		print 'dir = None'
	else:
		dir = args.dstdir[0]
#		print 'dir=', dir
	
	try:
		os.chdir( dir )
	except:
		eprint( 'download.py: failed to change directory to %s' % dir )
		return 1

	for url in urls:
		try:
			download( url )
		except:
			eprint( 'download.py: failed to download %s' % url )

if __name__ == "__main__":
	main()
