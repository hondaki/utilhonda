#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>
#include<math.h>

#include"utilHonda.h"

void perr_warning( const char *message, int code )
{
	char	buf[256];
	snprintf( buf, 256, "%s: %d", message, code );
	perror( buf );
}
void perr_warning2( const char *component, const char *message, int code )
{
	char	buf[512];
	snprintf( buf, 512, "%s: %s", component, message );
	perr_warning( buf, code );
}
void perr_exit( const char *message, int code )
{
	perr_warning( message, code );
	exit( code );
}
void perr_exit2( const char *component, const char *message, int code )
{
	perr_warning2( component, message, code );
	exit( code );
}
void err_exit( const char *message, int code )
{
	fprintf(stderr,"%s : %d\n", message, code );
	exit(code);
}

void err_exit2( const char *component, const char *message, int code )
{
	fprintf(stderr,"%s: ", component);
	err_exit( message, code );
}
void err_warning2( const char *component, const char *message, int code )
{
	char	buf[512];
	snprintf( buf, 512, "%s: %s", component, message );
	err_warning( buf, code );
}
void err_warning( const char *message, int code )
{
	fprintf( stderr, "%s: %d\n", message, code );
}

// Old Type
void erExit( const char *message, int code )
{
	err_exit( message, code );
}


char *rmLeadingBlanks( char *str, const char *blanks )
{
	int	len;
	len=strlen(str);
	while(  *str != '\0' && strchr( blanks, *str ) ){
		memmove( str, str+1, len-1);
		*(str+len-1)='\0';
		len--;
	}
	return str;
}

char *rmTrailingBlanks( char *str, const char *blanks )
{
	char	*pt;
	if( *str == '\0' )
		return str;

	pt = str+strlen(str)-1;
	while(  pt > str && strchr( blanks, *pt ) ){
		*pt = '\0';
		pt--;
	}
	return str;
}

char *trim( char *str, const char *blanks )
{
	rmLeadingBlanks(str, blanks);
	rmTrailingBlanks(str, blanks);

	return str;
}

static char *rm1c( char *str )
{
	int	len;
	len = strlen(str);
	if( len == 0 )
		return str;
	memmove( str, str+1, len-1);
	*(str+len-1)='\0';
	return str;
}

char *packBlanks( char *str, const char *blanks )
{

	char	*pt;
	int sw = 0;

	pt = trim(str, blanks);
	while( *pt != '\0'){
		if( *(pt+1) == '\0' )
			break;
		if( strchr( blanks, *(pt+1)) ){
			if( sw ){
				rm1c( pt );
			} else {
				pt++;
			}
			sw = 1;
		} else {
			sw = 0;
			pt++;
		}
	}
	return str;
}

int removeExtension( char *fname, const char *extension )
{

	int	l, le;

	l = strlen( fname );
	le = strlen( extension );
	if(  l  < le + 1 )
		return -1;
	if( _stricmp( fname + l - le, extension ) )
		return -1;

	fname[ l-le ] = 0;

	return 0;
}

int	tokens( char *str, char **argv, char *separators, int margc )
{
	int	argc = 0;

#if defined(WINDOWS) || defined(WIN32) || defined(WIN64)
	char 	*pt = str;
	while( (*argv=strtok(pt, separators)) != NULL ){
		if( **argv != '\0'){
			argc++;
			argv++;
			if( argc == margc )
				break;
		}
		pt = NULL;
	}
#else
	while( (*argv=strsep(&str, separators)) != NULL ){
		if( **argv != '\0'){
			argc++;
			argv++;
			if( argc == margc )
				break;
		}
	}
#endif
	return argc;
}

char *pureFileName( char *fname )
{
	char	*pt;
	pt = strrchr(fname, DirectorySeparater );
	if( pt != NULL )
		return pt+1;
	else
		return	fname;
}

int haveExtension ( const char *fname, const char *extension )
{
	const char	*pt;
	pt= fname + strlen( fname );
	while( pt > fname ){
		if( *pt == '.' ) {
			if( *extension != '.' )
				pt++;
			if( !_stricmp( pt, extension) )
				return 1;
			else
				return 0;
		}
		pt--;
	}
	return 0;
}

	static int	counterTotal;
	static int	current;
	static int	currentP;

	void	counterSet( int total )
	{
		counterTotal = total;
		current = 0;
		currentP = 0;
		printf("0");
	}

	void	counterDisp( int count )
	{
		int	p;

//		printf("%d/%d ", count, counterTotal );
		p = (int)((double) count / counterTotal * 50 + 0.001);
		while( currentP < p ){
			currentP++;
			if( currentP % 5 == 0 )
				printf("%d", currentP/5 % 10);
			else
				printf("+");
		}
		if( currentP >= 50 ) {
			printf("\n");
		}
	}

#if defined (MAIN)
int main()
{
	char	str[1024];
	char	*argv[256];
	int	argc;
	int	i;

	strcpy( str, "  a   b   \t\t  c asd\nasdf  ");
//	printf( "[%s]\n", packBlanks( str, BLANKS) );
	printf( "[%s]\n", str );

	strcpy( str, "  a,   b   \t\t  c asd\nasdf  ");
	argc = tokens(  str, argv, SEPARATORS, 256 );
	for(i=0;i<argc;i++){
		printf("%d [%s]\n", i, argv[i] );
	}
	
	return 0;
}
#endif

vect *vectRotzLeft90( vect *x )
{
	vect	wk;
	
	wk = *x;
	x->x = -wk.y;
	x->y = wk.x;
	return x;
}
vect *vectRotzRight90( vect *x )
{
	vect	wk;
	
	vectCopy( &wk, x );
	x->x = wk.y;
	x->y = -wk.x;
	return x;
}

vect *vectCopy( vect *vdest, vect *vsrc )
{
	vdest->x = vsrc->x;
	vdest->y = vsrc->y;
	vdest->z = vsrc->z;
	return vdest;
}

vect *vectDup( vect *vsrc )
{
	vect	*vdest;
	vdest = (vect*) malloc( sizeof( *vdest ) );
	*vdest = *vsrc;
	return vdest;
}

vect *vectConstruct ( vect *vdest, point *p )
{
	vdest->x = p->x;
	vdest->y = p->y;
	vdest->z = p->z;
	return vdest;
}
vect *vectConstruct2 ( vect *vdest, point *p1, point *p2  )
{
	vdest->x = p2->x - p1->x;
	vdest->y = p2->y - p1->y;
	vdest->z = p2->z - p1->z;
	return vdest;
}

vect *vectDiagonal( vect *v, vect *v1, vect *v2 )
{
	double	x1,y1,z1,x2,y2,z2,x,y,z;
//	vect vdest;
	x1 = v1->x;	y1 = v1->y; z1 = v1->z;
	x2 = v2->x;	y2 = v2->y; z2 = v2->z;
	x =  (y1*z2-z1*y2);
	y =  (z1*x2-x1*z2);
	z =  (x1*y2-y1*x2);

	v->x = x;
	v->y = y;
	v->z = z;
	vectNormalize(v);
	return v;
}

double	vectAbsolute( vect *x )
{
	return sqrt( x->x*x->x + x->y*x->y + x->z*x->z);
}

vect	*vectNormalize( vect *x  )
{
	double	f;
	if( (f = vectAbsolute(x) ) == 0. )
		err_exit( (char * ) "vectNormalize: the size is 0\n", 1);

	return vectScale( x, 1.0/f );
}

vect	*vectFlip( vect *x )
{
		x->x = -x->x;
		x->y = -x->y;
		x->z = -x->z;
		return x;
}
vect * vectScale ( vect *x, double fact )
{
	x->x *= fact;
	x->y *= fact;
	x->z *= fact;

	return x;
}

/* inner product */
double	dotProd( vect *u, vect *v )
{
	return	u->x*v->x+u->y*v->y+u->z*v->z;
}

/* outer product p = u X v, p's direction is u->v->p is same as x,y,z */
vect	*outProd( vect *u, vect *v, vect *p )
{
	p->x = u->y*v->z - u->z*v->y;
	p->y = u->z*v->x - u->x*v->z;
	p->z = u->x*v->y - u->y*v->x;
	return p;
}

int removeExtension2( char *fname )
{

//	int	l;
	char	*pure, *extension;

//	l = strlen( fname );

	pure = pureFileName( fname );
	if( (extension = strrchr( pure, '.' )) == NULL ){
		return -1;
	} else if( pure == extension ) {
		return -1;
	} else {
		*extension = '\0';
	}
/*
	if( l > 4 && fname[l-4] == '.' ) {
		fname[l-4] = 0;
	} else if( l > 5 && fname[l-5] == '.' ){
		fname[l-5] = 0;
	} else {
		return -1;
	}
*/
	return 0;
}

char *dmsStrBuf( double x, int ndigit, char *buf )
{
	return strcpy( buf, dmsStr(x, ndigit ) );
}

char *dmsStr( double x, int ndigit )
{
	static char	buf[128];
	char	fmt[64];
	int	deg, min;
	double	sec;
	int	sign;

//	printf("dmsStr: %s\n", fmt );

	if( fabs(x) < 1.0e-9 )
		x = 0.0;

	if( x < 0.0 )
		sign = -1;
	else if( x == 0. )
		sign = 0;
	else
		sign = 1;

	if( sign == 0 ){
		deg = 0;
		min = 0;
		sec = 0.;
	} else {
		x = fabs( x );
		deg = floor(x);
		min = floor((x-deg)*60.);
		sec = (x-deg-min/60.)*3600.;
//		sec = ((x-deg)*60.-min)*60.;
	}

//	printf( "%lf %d %lf %d %lf\n", x, sign, (x-abs(deg))*60., min, (x-abs(deg))*60.-min);
//	printf( "dmsStr: %d %d %lf\n", deg, min, (x-deg)*60.-min );

	if( sign >= 0 ){
		if( ndigit <= 0 )
			strcpy( fmt, "%d:%2.2d:%2.0lf" );
		else
			sprintf( fmt, "%s%dlf", "%d:%2.2d:%.", ndigit );
	} else {
		if( ndigit <= 0 )
			strcpy( fmt, "-%d:%2.2d:%2.0lf" );
		else
			sprintf( fmt, "%s%dlf", "-%d:%2.2d:%.", ndigit );
	}


	sprintf(buf, fmt, deg, min, sec );		
		
//	printf("dmsStr: %s\n", buf );
	return buf;
}
