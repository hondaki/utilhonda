#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>
#include<math.h>

#include"image.h"
#include"imgio.h"

#define	MAINNAME	"hughTrans"

#include"utilHonda.h"
void usage();


hughTrans( u_char	**img, u_short **hugh, int wi, int hi, int wo, int ho,
int yBase, int a1, int a2, int theta1, int theta2, double thetaResolution )
{
	int	i, j, a, itheta;
	double	x, y, theta;
	double	tanTheta, tanTheta1, tanTheta2;
/*
	theta: angle from Y axis.
0 < a < w ( width of the original image )

x1 = colum
y1 = h-line
y: usual y coordinate from down to up.
Constant yBase
yBase is to avoid division by 0 to calculate angle theta.
a is the cross point with y=yBase

range is the allowance for detecting the line
such as 5 degree

tan(theta) = (x1-a)/(y1-yBase)

Plot (a,tan(theta))

range of a:			a1 < a < a2 to 0 to wo-1
range of tanTheta:	tan(theta1) < theta < tan(theta2) to 0 to ho-1

*/
	tanTheta1 = tan( theta1/180.*PI );
	tanTheta2 = tan( theta2/180.*PI );
	for( i=0; i<hi; i++ ){
		y = hi-i-1;
		for(j=0; j<wi; j++ ){
			x = j;
			if( img[i][j] == 0 )
				continue;
			for( a=a1; a<=a2; a++ ){
				tanTheta = (x-a)/(y-yBase);
				itheta = (tanTheta-tanTheta1)/( tanTheta2 - tanTheta1)*(ho-1);
//				theta = atan( tanTheta ) /PI*180.;
				if( itheta >= 0 && itheta < ho ) {
					hugh[itheta][a-a1]++;
				}
			}
		}
	}
}

main( int argc, char *argv[])
{
	char	*imgFNameI, imgFNameO[1024];
	int		a1, a2, theta1, theta2;
	int		wi, hi, wo, ho, yBase;
	int		thetaScale;

	IMAGE_FILE	*imgFI, *imgFO;
	IMAGE		*imgI,  *imgO;
	u_char		**xySpace;
	u_short		**hugh;

	if( argc < 2 ){
		usage();
		exit(1);
	}
	imgFNameI=argv[1];

	imgFI=image_file_open( imgFNameI, IMAGE_RDONLY, IMAGE_BUFFERED );
	if( imgFI == NULL ){
		fprintf(stderr, MAINNAME "image file open error: %s\n",imgFNameI );
		return 1;
	}

	strcpy( imgFNameO, imgFNameI);
	removeExtention2( imgFNameO );
	strcat( imgFNameO, "_hugh.hdr");

	wi = imgFI->w;
	hi = imgFI->h;
	xySpace = imgFI->image->data[0];

	a1 = 0;
	a2 = wi-1;

	theta1 = -30;
	theta2 =  30;
	thetaScale = 1;

	yBase = -hi*0.1;

	wo = a2-a1+1;
	ho = (theta2-theta1)*thetaScale+1;

	if(( imgO = image_alloc( wo, ho, IMAGE_USHORT, 1 ) ) == NULL ){
		fprintf( stderr, MAINNAME "%s hugh image allocate error %d %d\n", wo, ho );
		image_file_close( imgFI );
		return -1;
	}
	hugh = imgO->data[0];

	hughTrans( xySpace, hugh, wi, hi, wo, ho, yBase, a1, a2, theta1, theta2, thetaScale );

	imgFO = image_file_create( imgFNameO, IMAGE_TRUNC, 0, imgO );
	if( imgFO == NULL ){
		fprintf(stderr, MAINNAME": image file create error: %s\n",imgFNameO );
		return 1;
	}
	image_file_close( imgFO );
	image_file_close( imgFI );
	image_destroy( imgO );

	return 0;
}


void usage()
{
	fprintf(stderr, MAINNAME " imagefile\n"
					" output file name is imagefile_hugh: e.g. imag1.hdr -> img1_hugh.hdr");
}
