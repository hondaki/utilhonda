#include<stdio.h>
#include<ctype.h>

void usage(void)
{
	fprintf(stderr,"PhotoshopColorTable file\n");
}

unsigned char	r[256];
unsigned char	g[256];
unsigned char	b[256];

int main( int argc, char *argv[] )
{
	char	*fname;
	int	i;
	FILE	*fp;
	unsigned char	buf[1025];

	int	lim0=0,
		lim1=51,
		lim2=102,
		lim3=153,
		lim4=204,
		lim5=255;

	if( argc != 2 ){
		usage();
		return 1;
	}
	fname = argv[1];

	for(i=0;i<256;i++){
		if( i<=lim1 ){
			r[i]=0;
			g[i]=(double)(255-0)/(lim1-lim0)*(i-lim0);
			b[i]=255;
		} else if( i<=lim2 ) {
			r[i]=0;
			g[i]=255;
			b[i]=(double)(0-255)/(lim2-lim1)*(i-lim1)+255;
		} else if( i<=lim3 ) {
			r[i]=(double)(255-0)/(lim3-lim2)*(i-lim2);
			g[i]=255;
			b[i]=0;
		} else if( i<=lim4 ) {
			r[i]=255;
			g[i]=(double)(0-255)/(lim4-lim3)*(i-lim1)+255;
			b[i]=0;
		} else {
			r[i]=255;
			g[i]=0;
			b[i]=(double)(255-0)/(lim5-lim4)*(i-lim4);
		}
	}
	fp = fopen( fname, "wb" );
	for(i=0;i<256;i++){
		fwrite( &r[i], 1, 1, fp );
		fwrite( &g[i], 1, 1, fp );
		fwrite( &b[i], 1, 1, fp );
	}
	fclose(fp);
}