int dayOfYear( int year, int month, int day )
{
	int doy;

	static int md[12] ={31,28,31,30,31,30,31,31,30,31,30,31};
	static int mdl[12]={31,29,31,30,31,30,31,31,30,31,30,31};
	int *mdp;

	int i;
	
	if(	( year % 4 ) == 0 && ( year % 100 ) != 0 || ( year % 400 ) == 0 )
		mdp = mdl;
	else
		mdp = md;

	doy=0;
	for(i=0;i<month-1;i++)
		doy += mdp[i];
	doy += day;

	return doy
}

main()
{
	int year, month, day;

	while(1){
		scanf("%d %d %d", &year, &month, &day);
		printf("%d\n", dayOfYear( year, month, day );
	}

}
