# README #

Convenient C routines

### What is this repository for? ###

* Quick summary
    * Utility Routines in C and python

* Version
    + 2.0

### How do I get set up? ###

* compile
    * make

* install
    * make install
        * See makefile before make install, especially to see target directory.

* download, download.py
	* download files from url
		* usage: download [-h] [-dstdir destination_directory]  url [url...] 

https://bitbucket.org/tutorials/markdowndemo